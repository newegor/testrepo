ECHO Get versions output
FOR /F "tokens=* USEBACKQ" %%F IN (`python scripts\extract_version.py`) DO (SET VERSION=%%F)
ECHO %VERSION%
ECHO Apply versions
python scripts\patch_version.py -f version.c -s "(?<=const char version_logo \[\] @ \".version\" = \").*(?=\";)" -v %VERSION%
ECHO Version applied
