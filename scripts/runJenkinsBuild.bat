rem Update revision nnumber before build
call scripts\bump_version.bat

rem Run all configuration builds
call "D:\Program Files (x86)\IAR Systems\Embedded Workbench 8.2\common\bin\iarbuild.exe" "%WORKSPACE%\iar\firstrepo.ewp" -build Debug -log all

rem Add revision numbers to output files
rem python scripts/output_converter.py