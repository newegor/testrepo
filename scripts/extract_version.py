import argparse
import subprocess
import json
import re

if __name__ == '__main__':

    # Configure parser
    args_parser = argparse.ArgumentParser(description="Scritp to extract a version from the GitVersion")

    # Parse arguments
    args = args_parser.parse_args()

    cmd = ['gitversion']
    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    versions = json.loads(result.stdout)

    pattern = 'Sha.'
    symbols_for_sha = 8
    version_to_extract = 'InformationalVersion'

    print('Extracting {}'.format(version_to_extract))

    out_str = versions[version_to_extract][:versions[version_to_extract].rfind(pattern) + len(pattern) + symbols_for_sha]
    print(out_str)
    exit(0)

