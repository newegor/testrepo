import os
import argparse
import re

if __name__ == '__main__':

    # Configure parser
    args_parser = argparse.ArgumentParser(description="Scritp to patch a version in the source file")
    args_parser.add_argument('-f', '--file', required=True, help='file to patch')
    args_parser.add_argument('-s', '--string', required=True, help='string to replace')
    args_parser.add_argument('-v', '--version', required=True, help='version to put')

    # Parse arguments
    args = args_parser.parse_args()

    # Check for the configuration file
    if os.path.isfile(args.file) is False:
        print("Error: file to patch is not found")
        exit(1)

    file_to_patch = args.file

    input_file = open(file_to_patch, 'r');
    input_text = input_file.read()
    input_file.close()

    result = re.findall(args.string,input_text)
    if len(result) != 1:
       print('Error: there are many or no hits for the input string')
       exit(1)

    print('Input string "{}" was found. Replacing with new version "{}"'.format(result[0],args.version))

    output_text = re.sub(args.string, args.version, input_text)

    output_file = open(file_to_patch, 'w');
    output_file.write(output_text)
    output_file.close()

    exit(0)