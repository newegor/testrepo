set(CMAKE_SYSTEM_NAME Generic)

set(CPU_FLAGS "--cpu Cortex-M3")

set(EWARM_ROOT_DIR "d:/Program Files (x86)/IAR Systems/Embedded Workbench 8.0/arm")

set(CMAKE_C_COMPILER "${EWARM_ROOT_DIR}/bin/iccarm.exe" "${CPU_FLAGS} -e")
set(CMAKE_CXX_COMPILER "${EWARM_ROOT_DIR}/bin/iccarm.exe" "${CPU_FLAGS} --c++")
set(CMAKE_ASM_COMPILER "${EWARM_ROOT_DIR}/bin/iasmarm.exe" "${CPU_FLAGS} -r")

set(LINKER_SCRIPT "\"${EWARM_ROOT_DIR}/config/linker/NXP/LPC1788.icf\"")
set(CMAKE_C_LINK_FLAGS "--semihosting --config ${LINKER_SCRIPT}")
set(CMAKE_CXX_LINK_FLAGS "--semihosting --config ${LINKER_SCRIPT}")

set(CMAKE_MAKE_PROGRAM "d:/Program Files (x86)/Microsoft Visual Studio 14.0/VC/bin/nmake.exe")
